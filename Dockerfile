ARG PYTHON_VERSION=latest

FROM python:${PYTHON_VERSION}

RUN pip install --upgrade pip setuptools \
    && pip install poetry
