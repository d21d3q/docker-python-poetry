# docker-python-poetry

Official [python](https://hub.docker.com/_/python) image with preinstalled poetry

**docker hub**: [d21d3q/python-poetry](https://hub.docker.com/r/d21d3q/python-poetry)

Motivation behind this image is to have image with preinstalled poetry to save
few seconds on CI.

CI for this repository is scheduled to rebuild images monthly
in order to install latest versions of poetry, pip and setuptools.

Python versions:
- `latest`
- `3.13`
- `3.12`
- `3.11`
- `3.10`
- `3.9`
- `3.8`
- `3.7`
- `3.6`

Packages installed (latest):
- `poetry`
- `pip`
- `setuptools`
